ifndef $(KERNELDIR)
	KERNELDIR=/lib/modules/`uname -r`/build
endif

EXTRA_CFLAGS = -O2
obj-y += subsystem/ controller/

all:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

clean:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) clean
	$(RM) Module.markers modules.order
