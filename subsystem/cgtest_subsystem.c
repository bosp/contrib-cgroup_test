#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/cgroup.h>

static struct cgroup_subsys_state *
	tcg_create(struct cgroup_subsys *, struct cgroup *);
static void
	tcg_destroy(struct cgroup_subsys *, struct cgroup *);
static int
	tcg_populate(struct cgroup_subsys *, struct cgroup *);
struct tcg_cgroup {
	struct cgroup_subsys_state css;
	u64 maxlen;
};

// Setup the new test CGroup subsystem
struct cgroup_subsys tcg_subsys = {
	.name = "cgtest",
	.create = tcg_create,
	.destroy = tcg_destroy,
	.populate = tcg_populate,
	.subsys_id = 5678,
	.use_id = 1,
	.module = THIS_MODULE,
};
EXPORT_SYMBOL(tcg_subsys);

static inline struct tcg_cgroup*
cgroup_to_tcg(struct cgroup *cgroup)
{
	return container_of(cgroup_subsys_state(cgroup, tcg_subsys.subsys_id),
			struct tcg_cgroup, css);
}

static u64
tcg_read_maxlen (struct cgroup *cgrp, struct cftype *cft)
{
	struct tcg_cgroup *tcg;
	tcg = cgroup_to_tcg(cgrp);
	return tcg->maxlen;
}

static int
tcg_write_maxlen(struct cgroup *cgrp, struct cftype *cft, u64 val)
{
	struct tcg_cgroup *tcg;
	tcg = cgroup_to_tcg(cgrp);
	tcg->maxlen = val;
	return 0;
}

struct cftype tcg_files[] = {
	{
		.name = "maxlen",
		.read_u64 = tcg_read_maxlen,
		.write_u64 = tcg_write_maxlen,
	}
};

static int
tcg_populate(struct cgroup_subsys *subsys, struct cgroup *cgroup)
{
	return cgroup_add_files(cgroup, subsys, tcg_files,
			ARRAY_SIZE(tcg_files));
	return 0;
}

static void
tcg_destroy(struct cgroup_subsys *subsys, struct cgroup *cgroup)
{

}

static struct cgroup_subsys_state *
tcg_create(struct cgroup_subsys *subsys, struct cgroup *cgroup)
{
	struct tcg_cgroup *tcg;
	tcg = kzalloc(sizeof(struct tcg_cgroup), GFP_KERNEL);
	if (!tcg)
		return ERR_PTR(-ENOMEM);
	tcg->maxlen = 3;
	return &tcg->css;
}

static int
__init init_cgroup_tcg(void)
{
	return cgroup_load_subsys(&tcg_subsys);
}

static void
__exit exit_cgroup_tcg(void)
{
	cgroup_unload_subsys(&tcg_subsys);
}

module_init(init_cgroup_tcg);
module_exit(exit_cgroup_tcg);

MODULE_AUTHOR ("Massimo Maggi and Patrick Bellasi");
MODULE_DESCRIPTION ("Test cgroup");
MODULE_LICENSE ("GPL");
