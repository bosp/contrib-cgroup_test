
	.:: Custom Control Group Example ::.

That's a simple example to show how to setup a custom control group subsystem"
and a corresponding "controller".  An overall introduction on control group
could be found in Linux kernel documentation:
http://lxr.linux.no/linux+v3.1.4/Documentation/cgroups/cgroups.txt

Is short, it is sufficient to know that: a *subsystem* is a module that makes
use of the task grouping facilities provided by cgroups to treat groups of
tasks in particular ways. A subsystem is typically a "resource controller" that
schedules a resource or applies per-cgroup limits, but it may be anything that
wants to act on a group of processes, e.g. a virtualization subsystem.

In this document, to better expose some concepts, we define *controller* the
specific component of a subsystem which is in charge to "use" information
defined by means of the cgrooup subsystem in order to achive a certain control
goal.

==== Example goal

An effective yet simple example of cgroup usage is the one we selected for this
tutorial.  Basically we want to provide user-sapce applications these features:
- write to a char device a string that is expected to be reported on kernel log
  buffer
- limit size of the string being reported to a value defined by a cgroup
  attribute.

To achieve such goals we need to:
1. define a new subsystem which allows to associate a "maxlen" attribute to
user-space processes.
2. provide a kernel log writing facility (e.g. a simple char device) which
checks this attribute in order to know how long the string to be logged should
be.

==== Setting up a new CGroup Subsystem

Each cgroup is exposed to user-sapce by means of an entry into a
virtual-filesystem. The user (with SYSADMIN capabilities) could mount this filesystem to:
- create subfolders: which correspond to the task grouping we want to define
- update the attributed exposed by the subsystem (i.e. in our example the value
  of maxlen)

In order to define a new control group we basically need to register a new
"struct cgroup_subsys" by provinding the required callbacks to create/destroy
and polpulate a suitable cgroup filesystem entry for this subsystem.

This piece of code is provided by the kernel module coded into:
subsystem/cgtest_subsystem.c

==== Using CGroup attrinutes

Once a task has been assigned (i.e. grouped) within a cgroup you could recever
cgroup attributes assigned to this task from two different places:
1. user-space: by simply reading the attribute files we used to assign tasks to
the cgroup
2. kernel-space: by getting a reference to the cgroup data structures which are
managed by the kernel for each task.

In order to demostrate how to used the "maxlen" attribute provided by the
previously defined subsystem, in this tutorial we make used of a char device.
Once a user-space task write a string into this simple char device, such string
is echoed into the kernel log being (eventually) truncated to the size
specified by the "maxlen" attribute defined by the cgroup the specific
user-space task belongs to.


==== Example modules compilation

To used the provided code, just run a compilation by specifing the kernel
source directory of the kernel you will load the modules into:

$ make KERNELDIR=<kernel_build_dir>

if everything compiles fine (it should :-)) you get a couple of modules to be loaded:

$ sudo insmod subsystem/cgtest_subsystem.ko
$ sudo insmod controller/cgtest_controller.ko

The first module should registers into the Linux kernel the new subsytem, which
could be verified by try to mount it:

$ sudo mount -t cgroup -o cgtest cgtest /tmp/cgtest

If everything goes fine, we should be able to see the "maxlen" attribute under
the used mountpoint (i.e. /tmp/cgtest in this example).

The second module defines a new chardevice, look for the assigned major device
number looking within /proc/devices, e.g.:

$ grep cgtest /proc/devices
$ 249 cgtest_consumer

and the build the device node to use for the testing, e.g.:

$ sudo mknod /tmp/cgtest_controller c 249 0

That should be enough to start experimenting with our new cgroup.

==== Example usage session

Ecoing a strings into the "controller" char device, by default only 3 chars
should be printed:

# echo -n 123456789 >/tmp/cgtest_controller
<kernel-log> PID[11537]: [123] (truncated by CGroup contraint, maxlen=3)

Let's check the configured output size: should be 3 chars
# cat /tmp/cgtest/cgtest.maxlen
3

Let's increase the configured output size to 5 chars
# echo 5 > /tmp/cgtest/cgtest.maxlen
# cat /tmp/cgtest/cgtest.maxlen
3

# echo -n 123456789 >/tmp/cgtest_controller
<kernel-log> PID[11537]: [12345] (truncated by CGroup contraint, maxlen=5)

# echo -n 123 >/tmp/cgtest_controller
<kernel-log> PID[11537]: [123]

Let's build two new sub-cgroups with different output constraints
# mkdir /tmp/cgtest/l2
# echo 2 > /tmp/cgtest/l2/cgtest.maxlen
# mkdir /tmp/cgtest/l6
# echo 6 > /tmp/cgtest/l6/cgtest.maxlen

Let's assign this shell into group "l2"
# echo $$ > /tmp/cgtest/l2/tasks

Now this shell should print only 2 chars...
# echo -n 123456789 >/tmp/cgtest_controller
<kernel-log> PID[11537]: [12] (truncated by CGroup contraint, maxlen=2)

Now moving this shell into group "l6"
# echo $$ > /tmp/cgtest/l6/tasks

Now this shell should print only 6 chars...
# echo -n 123456789 >/tmp/cgtest_controller
<kernel-log> PID[11537]: [123456] (truncated by CGroup contraint, maxlen=6)

