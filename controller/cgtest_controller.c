#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <asm/current.h>
#include <linux/cgroup.h>

//===== [ Pseudo-include start]=====
// That should go into a proper header file for the custom subsystem we
// are demostrating here. That's dirty, but that's just a demo ;-)
struct tcg_cgroup {
	struct cgroup_subsys_state css;
	u64 maxlen;
};
extern struct cgroup_subsys tcg_subsys;
static inline struct tcg_cgroup *cgroup_to_tcg(struct cgroup *cgroup)
{
	return container_of(cgroup_subsys_state(cgroup, tcg_subsys.subsys_id),
			struct tcg_cgroup, css);
}
//=====[ Pseudo include end]=====

static ssize_t
device_write(struct file *filp, const char *buff, size_t bytes, loff_t * off)
{
	struct cgroup *cg = task_cgroup(get_current(), tcg_subsys.subsys_id);
	struct tcg_cgroup *tcg = cgroup_to_tcg(cg);
	size_t len = bytes;
	char* mybuf;


	printk(KERN_ERR "CGTest WRITE (ml: %llu)...\n", tcg->maxlen);

	// Copy string for echoing
	mybuf = (char*)kmalloc(bytes + 1, GFP_KERNEL);
	copy_from_user(mybuf, buff, bytes);
	mybuf[bytes] = 0;

	// Check length constraints
	if (bytes <= tcg->maxlen) {
		printk(KERN_ERR "PID[%d]: [%s]\n", get_current()->pid, mybuf);
	} else {
		// Truncate string if longer than allowed
		mybuf[tcg->maxlen] = 0;
		printk(KERN_ERR "PID[%d]: [%s] (truncated by CGroup contraint, "
			"maxlen=%llu)\n)", get_current()->pid, mybuf, tcg->maxlen);
	}

	// Release all resources
	kfree(mybuf);

	printk(KERN_ERR "DONE!\n");

	// Discarding all exceeding buffer chars
	return bytes;
}

static struct file_operations fops = {
	.write = device_write,
};

static int m;
static int
__init module_load (void)
{
	m = register_chrdev(0, "cgtest_controller", &fops);
	printk("CGroup test (controller) chrdev, mjr: %d\n", m);
	return 0;
}

static void
__exit module_unload (void)
{
	unregister_chrdev(m, "cgtest_controller");

}

module_init (module_load);
module_exit (module_unload);

MODULE_AUTHOR ("Massimo Maggi and Patrick Bellasi");
MODULE_DESCRIPTION ("Simple chardev for CGroup Testing");
MODULE_LICENSE ("GPL");

